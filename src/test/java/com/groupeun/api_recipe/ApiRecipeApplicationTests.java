package com.groupeun.api_recipe;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.groupeun.api_recipe.dto.RecipeDto;
import com.groupeun.api_recipe.dto.StepDto;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ApiRecipeApplicationTests {

	private String endpoint = "/services/recipe";
	private String token;
	private RecipeDto dto1;

	private RestTemplate keycloakClient;
	@Autowired
	private MockMvc mvc;
	@Autowired
	private ObjectMapper objectMapper;
	// @Autowired
	// private JsonMapper jsonMapper;

	@PostConstruct
	private void initialize () {
		dto1 = new RecipeDto();
		dto1.setName("Recipe 1");
		dto1.setDescription("Recipe test");
		dto1.setAuthorId(UUID.randomUUID());
		dto1.setNutriScore("A");
		dto1.setPreparationTime(15);

		StepDto stepDto = new StepDto();
		stepDto.setStep(1);
		stepDto.setIngredientId(UUID.randomUUID());
		stepDto.setDescription("Step 1");
		dto1.getSteps().add(stepDto);
		stepDto = new StepDto();
		stepDto.setStep(2);
		stepDto.setIngredientId(UUID.randomUUID());
		stepDto.setDescription("Step 2");
		dto1.getSteps().add(stepDto);

		token = getAuthentificationToken();
	}

	private String getAuthentificationToken() {
		// Get authentification token
		keycloakClient = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("client_id", "recipe-app");
		map.add("username", "pierre.dupont@test.com");
		map.add("password", "azerty");
		map.add("grant_type", "password");

		HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

		ResponseEntity<Map> response = keycloakClient.exchange("http://api-user.haingue.com/realms/cookeasy/protocol/openid-connect/token",
			HttpMethod.POST,
			entity,
			Map.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		return "Bearer " + String.valueOf(response.getBody().get("access_token"));
	}

	@Test
	@Order(1)
	void contextLoads() throws Exception {
		assertThat(mvc).isNotNull();
		assertThat(objectMapper).isNotNull();
		// assertThat(jsonMapper).isNotNull();
		assertThat(dto1).isNotNull();
		assertThat(token).isNotNull();
		assertThat(token).isNotEmpty();
	}

	@Test
	@Order(2)
	void getRandomRecipeWithEmptyDatabase () throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/services/recipe"))
		.andExpect(status().isNoContent());
	}

	@Test
	@Order(3)
	void postNewRecipe () throws Exception {
		MvcResult result = mvc.perform(MockMvcRequestBuilders.post(endpoint)
				.header(HttpHeaders.AUTHORIZATION, token)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(dto1)))
		.andExpect(status().isCreated())
		/*.andDo(result -> {
			RecipeDto dtoResult = objectMapper.convertValue(result.getResponse().getContentAsString(), RecipeDto.class);
			dto1.setId(dtoResult.getId());
		})*/
		.andReturn();
//		RecipeDto dtoResult = objectMapper.readValue(result.getResponse().getContentAsString(), RecipeDto.class);
//		RecipeDto dtoResult = jsonMapper.readValue(result.getResponse().getContentAsString(), RecipeDto.class);
//		dto1.setId(dtoResult.getId());
//		assertThat(dto1).isEqualTo(dtoResult);
	}

	@Test
	@Order(4)
	void getRandomRecipe () throws Exception {
		mvc.perform(MockMvcRequestBuilders.get(endpoint))
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.id", CoreMatchers.notNullValue()))
//				.andExpect(jsonPath("$.id", CoreMatchers.is(dto1.getId())))
				.andExpect(jsonPath("$.name", CoreMatchers.is(dto1.getName())));
	}

}
