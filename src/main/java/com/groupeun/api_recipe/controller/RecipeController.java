package com.groupeun.api_recipe.controller;

import java.security.Principal;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupeun.api_recipe.dto.RecipeDto;
import com.groupeun.api_recipe.exception.ApiRecipeException;
import com.groupeun.api_recipe.service.RecipeService;

import org.keycloak.KeycloakSecurityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/services/recipe")
public class RecipeController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private RecipeService recipeService;

    @PostMapping
    public ResponseEntity addRecipe (@RequestBody RecipeDto recipeDto, HttpServletRequest request, HttpServletResponse response) {
        logger.info("Add new recipe {}: {}", request.getAttribute(KeycloakSecurityContext.class.getName()), recipeDto.getName());
        try {
            return new ResponseEntity(recipeService.save(recipeDto), HttpStatus.CREATED);
        } catch (ApiRecipeException e) {
            logger.debug(e.toString());
            return e.toResponseEntity(response);
        }
    }

    @PutMapping
    public ResponseEntity updateRecipe (@RequestBody RecipeDto recipeDto, HttpServletResponse response, Principal principal) {
        logger.info("Update recipe by {}: {}", principal.getName(), recipeDto.getId());
        try {
            return ResponseEntity.ok(recipeService.update(recipeDto));
        } catch (ApiRecipeException e) {
            logger.debug(e.toString());
            return e.toResponseEntity(response);
        }
    }

    @DeleteMapping
    public ResponseEntity deleteRecipe (@RequestBody RecipeDto recipeDto, HttpServletResponse response) {
        logger.info("Delete recipe: {}", recipeDto.getId());
        try {
            recipeService.delete(recipeDto.getId());
            return ResponseEntity.ok().build();
        } catch (ApiRecipeException e) {
            logger.debug(e.toString());
            return e.toResponseEntity(response);
        }
    }
}
