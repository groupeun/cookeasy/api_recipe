package com.groupeun.api_recipe.controller;

import com.groupeun.api_recipe.exception.NoDataException;
import com.groupeun.api_recipe.service.RecipeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@Controller
@RequestMapping("/services/recipe")
public class FetchRecipeController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private RecipeService recipeService;

    @GetMapping
    public ResponseEntity getRandomRecipe (HttpServletResponse response) {
        logger.info("Load random recipe");
        try {
            return ResponseEntity.ok(recipeService.getRandomRecipe());
        } catch (NoDataException e) {
            logger.debug(e.toString());
            return e.toResponseEntity(response);
        }
    }

    @GetMapping("/name/{name}")
    public ResponseEntity getRecipeByName (@PathVariable String name, HttpServletResponse response) {
        logger.info("Load one recipe by name: {}", name);
        try {
            return ResponseEntity.ok(recipeService.getOneByName(name));
        } catch (NoDataException e) {
            logger.debug(e.toString());
            return e.toResponseEntity(response);
        }
    }

    @GetMapping("/id/{id}")
    public ResponseEntity getRecipeById (@PathVariable String id, HttpServletResponse response) {
        logger.info("Load one recipe by id: {}", id);
        try {
            return ResponseEntity.ok(recipeService.getOneById(UUID.fromString(id)));
        } catch (NoDataException e) {
            logger.debug(e.toString());
            return e.toResponseEntity(response);
        }
    }
}
