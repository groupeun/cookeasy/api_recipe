package com.groupeun.api_recipe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiRecipeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiRecipeApplication.class, args);
	}

}
