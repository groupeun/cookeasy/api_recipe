package com.groupeun.api_recipe.exception;

import org.springframework.http.HttpStatus;

public class NotExistException extends ApiRecipeException {

    public NotExistException(String entity) {
        super("The entity does not exist: " + entity, HttpStatus.BAD_REQUEST);
    }
}
