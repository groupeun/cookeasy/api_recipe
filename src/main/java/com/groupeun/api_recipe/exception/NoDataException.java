package com.groupeun.api_recipe.exception;

import org.springframework.http.HttpStatus;

public class NoDataException extends ApiRecipeException {

    public NoDataException() {
        super(HttpStatus.NO_CONTENT);
    }
}
