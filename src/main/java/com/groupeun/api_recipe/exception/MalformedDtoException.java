package com.groupeun.api_recipe.exception;

import org.springframework.http.HttpStatus;

public class MalformedDtoException  extends ApiRecipeException {

    private String dto;

    public MalformedDtoException(String dto) {
        super("Recipe dto is malformed", HttpStatus.BAD_REQUEST);
        this.dto = dto;
    }

    @Override
    public String toString() {
        return "ApiRecipeException{" +
                "dto=" + dto +
                "message=" + getMessage() +
                '}';
    }
}
