package com.groupeun.api_recipe.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletResponse;

public class ApiRecipeException extends Exception {

    private HttpStatus httpStatus;

    public ApiRecipeException() {
        httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    public ApiRecipeException(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public ApiRecipeException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    public ApiRecipeException(String message, Throwable cause, HttpStatus httpStatus) {
        super(message, cause);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public ResponseEntity toResponseEntity(HttpServletResponse response) {
        response.setHeader("Error", getMessage());
        return new ResponseEntity(httpStatus);
    }

    @Override
    public String toString() {
        return "ApiRecipeException{" +
                "httpStatus=" + httpStatus +
                "message=" + getMessage() +
                '}';
    }
}
