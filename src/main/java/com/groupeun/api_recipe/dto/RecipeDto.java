package com.groupeun.api_recipe.dto;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class RecipeDto {

    private UUID id;
    private String name;
    private String description;
    private String nutriScore;
    private Integer preparationTime;
    private UUID authorId;
    private Set<StepDto> steps;

    public RecipeDto() {
        this.steps = new HashSet<>();
    }

    public RecipeDto(UUID id, String name, String description, String nutriScore, Integer preparationTime, UUID authorId, Set<StepDto> steps) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.nutriScore = nutriScore;
        this.preparationTime = preparationTime;
        this.authorId = authorId;
        this.steps = steps;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNutriScore() {
        return nutriScore;
    }

    public void setNutriScore(String nutriScore) {
        this.nutriScore = nutriScore;
    }

    public Integer getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(Integer preparationTime) {
        this.preparationTime = preparationTime;
    }

    public UUID getAuthorId() {
        return authorId;
    }

    public void setAuthorId(UUID authorId) {
        this.authorId = authorId;
    }

    public Set<StepDto> getSteps() {
        return steps;
    }

    public void setSteps(Set<StepDto> steps) {
        this.steps = steps;
    }

    public boolean add (StepDto step) {
        return this.steps.add(step);
    }

    public boolean remove (StepDto step) {
        return this.steps.remove(step);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecipeDto recipeDto = (RecipeDto) o;
        return id.equals(recipeDto.id) && Objects.equals(name, recipeDto.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "RecipeDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", nutriScore='" + nutriScore + '\'' +
                ", preparationTime=" + preparationTime +
                ", authorId=" + authorId +
                ", steps=" + steps +
                '}';
    }
}
