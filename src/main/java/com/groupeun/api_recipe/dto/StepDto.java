package com.groupeun.api_recipe.dto;

import java.util.Objects;
import java.util.UUID;

public class StepDto {

    private UUID ingredientId;
    private Integer step;
    private String description;

    public StepDto() {
    }

    public StepDto(UUID ingredientId, Integer step, String description) {
        this.ingredientId = ingredientId;
        this.step = step;
        this.description = description;
    }

    public UUID getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(UUID ingredientId) {
        this.ingredientId = ingredientId;
    }

    public Integer getStep() {
        return step;
    }

    public void setStep(Integer step) {
        this.step = step;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StepDto stepDto = (StepDto) o;
        return ingredientId.equals(stepDto.ingredientId) && step.equals(stepDto.step);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ingredientId, step);
    }

    @Override
    public String toString() {
        return "StepDto{" +
                "ingredientId=" + ingredientId +
                ", step=" + step +
                ", description='" + description + '\'' +
                '}';
    }
}
