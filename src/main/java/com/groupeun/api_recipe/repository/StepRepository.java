package com.groupeun.api_recipe.repository;

import com.groupeun.api_recipe.model.StepEntity;
import com.groupeun.api_recipe.model.id.StepId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StepRepository extends CrudRepository<StepEntity, StepId> {
}
