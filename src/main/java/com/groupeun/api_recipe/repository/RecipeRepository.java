package com.groupeun.api_recipe.repository;

import com.groupeun.api_recipe.model.RecipeEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface RecipeRepository extends CrudRepository<RecipeEntity, UUID> {

    Optional<RecipeEntity> getFirstByNameOrderByName (String name);

    Page<RecipeEntity> getAllByOrderByName (Pageable page);
    Page<RecipeEntity> getAllByNameLikeOrderByName (String name, Pageable page);

}
