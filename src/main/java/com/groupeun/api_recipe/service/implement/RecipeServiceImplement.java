package com.groupeun.api_recipe.service.implement;

import com.groupeun.api_recipe.dto.RecipeDto;
import com.groupeun.api_recipe.dto.StepDto;
import com.groupeun.api_recipe.exception.ApiRecipeException;
import com.groupeun.api_recipe.exception.MalformedDtoException;
import com.groupeun.api_recipe.exception.NoDataException;
import com.groupeun.api_recipe.exception.NotExistException;
import com.groupeun.api_recipe.model.RecipeEntity;
import com.groupeun.api_recipe.model.StepEntity;
import com.groupeun.api_recipe.model.id.StepId;
import com.groupeun.api_recipe.repository.RecipeRepository;
import com.groupeun.api_recipe.service.RecipeService;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RecipeServiceImplement implements RecipeService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private RecipeRepository recipeRepository;

    @Override
    public RecipeDto getRandomRecipe() throws NoDataException {
        Random rd = new Random();
        int randomNumber = Long.valueOf(recipeRepository.count()).intValue();
        if (randomNumber > 0) {
            randomNumber = Math.abs(rd.nextInt(randomNumber));
            Page<RecipeEntity> result = recipeRepository.getAllByOrderByName(PageRequest.of(randomNumber, 1));
            Optional<RecipeEntity> firstResult = result.get().findFirst();
            if (firstResult.isPresent()) {
                return convertToDto(firstResult.get());
            }
        }
        throw new NoDataException();
    }

    @Override
    public RecipeDto getOneByName(String name) throws NoDataException {
        Optional<RecipeEntity> firstResult = recipeRepository.getFirstByNameOrderByName(name);
        if (firstResult.isPresent()) {
            return convertToDto(firstResult.get());
        }
        throw new NoDataException();
    }

    @Override
    public RecipeDto getOneById(UUID id) throws NoDataException {
        Optional<RecipeEntity> firstResult = recipeRepository.findById(id);
        if (firstResult.isPresent()) {
            return convertToDto(firstResult.get());
        }
        throw new NoDataException();
    }

    public Page<RecipeDto> getAllPageable (Pageable page) {
        List<RecipeDto> dtoList = new ArrayList<RecipeDto>();
        Page<RecipeEntity> recipePages = recipeRepository.getAllByOrderByName(page);
        recipePages.stream().map(this::convertToDto).collect(Collectors.toCollection(() -> dtoList));
        return new PageImpl<RecipeDto>(dtoList, page, recipePages.getTotalElements());
    }

    public Page<RecipeDto> getAllByNamePageable (String name, Pageable page) {
        List<RecipeDto> dtoList = new ArrayList<RecipeDto>();
        Page<RecipeEntity> recipePages = recipeRepository.getAllByNameLikeOrderByName(name, page);
        recipePages.stream().map(this::convertToDto).collect(Collectors.toCollection(() -> dtoList));
        return new PageImpl<RecipeDto>(dtoList, page, recipePages.getTotalElements());
    }

    public RecipeDto save (RecipeDto recipeDto) throws ApiRecipeException {
        if (!isValidRecipe(recipeDto)) {
            throw new MalformedDtoException(RecipeDto.class.getName());
        }
        recipeDto.setId(UUID.randomUUID());
        RecipeEntity recipeEntity = recipeRepository.save(convertToModel(recipeDto));
        if (recipeEntity == null) {
            throw new ApiRecipeException("Error to save recipe", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return convertToDto(recipeEntity);
    }

    public RecipeDto update (RecipeDto recipeDto) throws ApiRecipeException {
        if (!isValidRecipe(recipeDto) || recipeDto.getId() == null) {
            throw new MalformedDtoException(RecipeDto.class.getName());
        }
        Optional<RecipeEntity> previousRrecipe = recipeRepository.findById(recipeDto.getId());
        if (!previousRrecipe.isPresent()) {
            throw new ApiRecipeException("Error to update recipe because the recipe does not exist", HttpStatus.BAD_REQUEST);
        }
        RecipeEntity recipeEntity = previousRrecipe.get();
        recipeEntity.setName(recipeDto.getName());
        recipeEntity.setDescription(recipeDto.getDescription());
        recipeEntity.setNutriScore(recipeDto.getNutriScore());
        recipeEntity.setPreparationTime(recipeDto.getPreparationTime());
        recipeEntity.setAuthorId(recipeDto.getAuthorId());
        recipeRepository.save(recipeEntity);
        return convertToDto(recipeEntity);
    }

    public void delete (RecipeDto recipe) throws ApiRecipeException {
        if (recipe.getId() == null) throw new ApiRecipeException("Recipe id is missing", HttpStatus.BAD_REQUEST);
        if (!recipeRepository.findById(recipe.getId()).isPresent()) throw new NotExistException(RecipeEntity.class.getName());
        recipeRepository.deleteById(recipe.getId());
    }

    public void delete (UUID id) throws NotExistException {
        if (!recipeRepository.findById(id).isPresent()) throw new NotExistException(RecipeEntity.class.getName());
        recipeRepository.deleteById(id);
    }

    private boolean isValidRecipe (RecipeDto dto) {
        return dto != null && !Strings.isEmpty(dto.getName())
            && dto.getAuthorId() != null && !dto.getSteps().isEmpty();
    }

    private RecipeDto convertToDto (RecipeEntity model) {
        RecipeDto dto = new RecipeDto();
        dto.setId(model.getId());
        dto.setName(model.getName());
        dto.setDescription(model.getDescription());
        dto.setAuthorId(model.getAuthorId());
        dto.setNutriScore(model.getNutriScore());
        dto.setPreparationTime(model.getPreparationTime());

        model.getSteps().forEach(stepModel -> {
            StepDto stepDto = new StepDto();

            stepDto.setIngredientId(stepModel.getId().getIngredientId());
            stepDto.setStep(stepModel.getStep());
            stepDto.setDescription(stepModel.getDescription());
            dto.add(stepDto);
        });
        return dto;
    }

    private RecipeEntity convertToModel (RecipeDto dto) {
        RecipeEntity model = new RecipeEntity();
        model.setId(dto.getId());
        model.setName(dto.getName());
        model.setDescription(dto.getDescription());
        model.setAuthorId(dto.getAuthorId());
        model.setNutriScore(dto.getNutriScore());
        model.setPreparationTime(dto.getPreparationTime());

        dto.getSteps().forEach(stepDto -> {
            StepEntity stepModel = new StepEntity();

            StepId id = new StepId();
            id.setRecipeId(dto.getId());
            id.setIngredientId(stepDto.getIngredientId());

            stepModel.setId(id);
            stepModel.setStep(stepDto.getStep());
            stepModel.setDescription(stepDto.getDescription());
            model.add(stepModel);
        });
        return model;
    }
}
