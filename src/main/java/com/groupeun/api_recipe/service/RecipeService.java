package com.groupeun.api_recipe.service;

import java.util.UUID;

import com.groupeun.api_recipe.dto.RecipeDto;
import com.groupeun.api_recipe.exception.ApiRecipeException;
import com.groupeun.api_recipe.exception.NoDataException;
import com.groupeun.api_recipe.exception.NotExistException;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface RecipeService {

    public RecipeDto getRandomRecipe () throws NoDataException;
    public RecipeDto getOneByName (String name) throws NoDataException;
    public RecipeDto getOneById (UUID id) throws NoDataException;

    public Page<RecipeDto> getAllPageable (Pageable page);

    public Page<RecipeDto> getAllByNamePageable (String name, Pageable page);

    public RecipeDto save (RecipeDto recipeDto) throws ApiRecipeException;

    public RecipeDto update (RecipeDto recipeDto) throws ApiRecipeException;

    public void delete (RecipeDto recipe) throws ApiRecipeException;

    public void delete (UUID id) throws NotExistException;

}
