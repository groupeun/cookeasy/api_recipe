package com.groupeun.api_recipe.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "recipe")
public class RecipeEntity {

    @Id
    private UUID id;
    @Column
    private String name;
    @Column
    private String description;
    @Column(length = 1)
    private String nutriScore;
    @Column
    private Integer preparationTime;
    @Column
    private UUID authorId;

    @OneToMany(mappedBy = "id.recipeId", cascade = CascadeType.ALL)
    private Set<StepEntity> steps;

    public RecipeEntity() {
        this.steps = new HashSet<>();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNutriScore() {
        return nutriScore;
    }

    public void setNutriScore(String nutriScore) {
        this.nutriScore = nutriScore;
    }

    public Integer getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(Integer preparationTime) {
        this.preparationTime = preparationTime;
    }

    public UUID getAuthorId() {
        return authorId;
    }

    public void setAuthorId(UUID authorId) {
        this.authorId = authorId;
    }

    public Set<StepEntity> getSteps() {
        return steps;
    }

    public boolean add (StepEntity step) {
        return this.steps.add(step);
    }

    public boolean remove (StepEntity step) {
        return this.steps.remove(step);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecipeEntity that = (RecipeEntity) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "RecipeEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", nutriScore='" + nutriScore + '\'' +
                ", preparationTime=" + preparationTime +
                ", authorId=" + authorId +
                '}';
    }
}
