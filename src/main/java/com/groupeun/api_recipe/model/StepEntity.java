package com.groupeun.api_recipe.model;

import com.groupeun.api_recipe.model.id.StepId;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "step")
public class StepEntity {

    @EmbeddedId
    private StepId id;
    @Column
    private Integer step;
    @Column
    private String description;

    public StepEntity() {
    }

    public StepId getId() {
        return id;
    }

    public void setId(StepId id) {
        this.id = id;
    }

    public Integer getStep() {
        return step;
    }

    public void setStep(Integer step) {
        this.step = step;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StepEntity that = (StepEntity) o;
        return id.equals(that.id) && Objects.equals(step, that.step) && Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, step, description);
    }

    @Override
    public String toString() {
        return "StepEntity{" +
                "id=" + id +
                ", step=" + step +
                ", description='" + description + '\'' +
                '}';
    }
}
