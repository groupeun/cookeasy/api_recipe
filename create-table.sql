CREATE TABLE recipe (  
    id UUID NOT NULL PRIMARY KEY COMMENT 'Primary Key',
    name VARCHAR(255),
    description VARCHAR(255),
    nutri_score VARCHAR(1),
    preparation_time int COMMENT 'preparation time (min)',
    author_id UUID COMMENT 'author id'
) DEFAULT CHARSET UTF8 COMMENT 'Recipe table';


CREATE TABLE step (  
    recipe_id UUID NOT NULL COMMENT 'recipe id',
    ingredient_id UUID NOT NULL COMMENT 'ingredient id',
    step int,
    description VARCHAR(255),
    CONSTRAINT PK_step PRIMARY KEY (recipe_id, ingredient_id),
    CONSTRAINT FK_recipe FOREIGN KEY (recipe_id) REFERENCES recipe(id)
) DEFAULT CHARSET UTF8 COMMENT 'Step table';
